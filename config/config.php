<?php

return array(
    'general' => array(
        'router' => 'Fruit\PuxRouter'
    ),

    'pux' => array(
        'mux' => implode(DIRECTORY_SEPARATOR, ['route', 'mux.php'])
    ),

    'twig' => array(
        'template_dir' => BASE_DIR . '/html',
        'debug'        => true,
        'cache_dir'    => '/tmp'
    ),
    
    'db' => array(
        'default' => array(
            'constr' => 'mysql:host=127.0.0.1;dbname=accounting;charset=utf8',
            'user'   => 'root',
            'pass'   => '1234'
        ),
    ),

    'upload' => array(
        'dir' => BASE_DIR . '/files',
        'prefix' => 'ul_'
    )
);
