<?php
//以下為route設定
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));

use Pux\Mux;

$mux = new Pux\Mux;


$mux->add(
    '/',
    ['Accounting\Controller\MainController', 'index']
);

//V3 route start
$mux->post('/v3/User/create', ['Accounting\Controller\V3UserController', 'create']);
$mux->post('/v3/User/select', ['Accounting\Controller\V3UserController', 'select']);
$mux->post('/v3/User/selectmoney', ['Accounting\Controller\V3UserController', 'selectmoney']);
$mux->post('/v3/User/delete', ['Accounting\Controller\V3UserController', 'delete']);
$mux->post('/v3/List/update', ['Accounting\Controller\V3ListController', 'update']);
$mux->post('/v3/Item/update', ['Accounting\Controller\V3CategoryController', 'update']);
//V3 route start

return $mux;
