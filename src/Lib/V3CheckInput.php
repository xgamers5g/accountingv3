<?php
namespace Accounting\Lib;

use Accounting\Lib\V3Lib;

class V3CheckInput
{
    /**
    * 檢查ID是否符合格式
    * @param $id 為輸入的ID
    * @return true or false
    */
    public static function checkId($id)
    {
        if (!is_null($id) && $id > 0) {
            $arr = array();
            $pattern = "/(\d){1,11}/";
            preg_match($pattern, $id, $arr);
            if (strcmp($id, $arr[0]) == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
    * 檢查時間是否符合格式
    * @param $date 為輸入的日期
    * @return true or false
    */
    public static function checkDate($date)
    {
        $arr = array();

        if (is_null($date)) {
            return false;
        }
        $pattern  = "/(\d{4,4})-((([1][0,2]|[0][1,3,5,7,8])-([0-2][1-9]|[3][0-1]))|".
                                "(([1][1]|[0][4,6,9])-([0-2][1-9]|[3][0]))|".
                                "(([0][2])-([0-1][1-9]|[2][0-9])))/";
        preg_match($pattern, $date, $arr);
        if (count($arr) > 0 && strcmp($date, $arr[0]) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * 檢查字串是否符合格式（不得輸入空格,逗號等特殊符號)
    * @param $str 為輸入的字串
    * @return true or die
    */
    public static function checkStr($str)
    {
        if (!is_null($str)) {
            $arr = array();
            $pattern = "~[^\x{4e00}-\x{9fa5}_a-zA-Z0-9]+~u";
            preg_match($pattern, $str, $arr);
            if (count($arr) == 0) {
                return true;
            } else {
                die("不得為空格或逗號等特殊符號");
            }
        }
    }
}
