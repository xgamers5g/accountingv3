<?php
namespace Accounting\Lib;

use Accounting\System\Controller;
use Accounting\Lib\V3CheckInput;
use Accounting\Model\V3Model;

class V3Lib extends Controller
{
    /**
    * 建構基本設定
    */
    public function __construct()
    {
        self::init();
    }

    /**
    * 判斷使用者輸入內容,格式正確才帶入執行 where
    */
    public static function userset()
    {
        self::init();
        if (self::$data === null || !isset(self::$data->userno)) {
            die("請輸入正確資料");
        }
        $uarr = array();

        if (V3CheckInput::checkId(self::$data->userno) == true) {
            $uarr["uid"] = self::$data->userno;
        }
        if (V3CheckInput::checkId(self::$data->listno) == true) {
            $uarr["lid"] = self::$data->listno;
        }
        if (V3CheckInput::checkStr(self::$data->listname) == true) {
            $uarr["lname"] = "\"".self::$data->listname."\"";
        }
        if (V3CheckInput::checkId(self::$data->money) == true) {
            $uarr["lamount"] = self::$data->money;
        }
        if (V3CheckInput::checkDate(self::$data->day) == true) {
            $uarr["ldatetime"] = "\"".self::$data->day."\"";
        }
        if (V3CheckInput::checkDate(self::$data->d1) == true) {
            $uarr["ldatetime1"] = "\"".self::$data->d1."\"";
        }
        if (V3CheckInput::checkDate(self::$data->d2) == true) {
            $uarr["ldatetime2"] = "\"".self::$data->d2."\"";
        }
        if (V3CheckInput::checkId(self::$data->itemno) == true) {
            $uarr["cid"] = self::$data->itemno;
        }
        if (V3CheckInput::checkStr(self::$data->itemname) == true) {
            $uarr["cname"] = "\"".self::$data->itemname."\"";
        }
        if (self::$data->itemtype == 1 or self::$data->itemtype == 2) {
            $uarr["ctype"] = self::$data->itemtype;
        }
        if (self::$data->usertype == 1 or self::$data->usertype == 2) {
            $uarr["utype"] = self::$data->usertype;
        }

        $tb = self::table();
        for ($i=0; $i<count($tb); $i++) {
            if (array_keys($tb)[$i] == self::$data->select) {
                $uarr["scolumn"] = $tb[array_keys($tb)[$i]];
            }
        }

        $col = self::column();
        for ($i=0; $i<count($col); $i++) {
            if (array_keys($col)[$i] == self::$data->group) {
                $uarr["group"] = $col[array_keys($col)[$i]];
            }
        }
        
        return $uarr;
    }

    /**
    * 判斷使用者輸入內容,格式正確才帶入執行 set  
    */
    public static function usersets()
    {
        self::init();
        if (self::$data === null) {
            die("請輸入正確資料");
        }
        $uarrs = array();
        if (V3CheckInput::checkStr(self::$data->listnames) == true) {
            $uarrs["lname"] = "\"".self::$data->listnames."\"";
        }
        if (V3CheckInput::checkId(self::$data->moneys) == true) {
            $uarrs["lamount"] = self::$data->moneys;
        }
        if (V3CheckInput::checkDate(self::$data->days) == true) {
            $uarrs["ldatetime"] = "\"".self::$data->days."\"";
        }
        if (V3CheckInput::checkId(self::$data->itemnos) == true) {
            $uarrs["cid"] = self::$data->itemnos;
        }
        if (V3CheckInput::checkStr(self::$data->itemnames) == true) {
            $uarrs["cname"] = "\"".self::$data->itemnames."\"";
        }
        if (self::$data->itemtypes == 1 or self::$data->itemtypes == 2) {
            $uarrs["ctype"] = self::$data->itemtypes;
        }
        return $uarrs;
    }

    /**
     * category table column 對應 user input column
     */
    public static function categorytable()
    {
        $carr = array(
            "cid" => "itemno",
            "cname" => "itemname",
            "ctype" => "itemtype"
            );
        return $carr;
    }

    /**
     * list table column 對應 user input column
     */
    public static function listtable()
    {
        $larr = array(
            "lid" => "listno",
            "cid" => "itemno",
            "lname" => "listname",
            "lamount" => "money",
            "ldatetime" => "day"
            );
        return $larr;
    }

    /**
     * DB table name 對應 user input table
     */
    public static function table()
    {
        $tarr = array(
            "user" => "user",
            "list" => "list",
            "item" => "category",
            );
        return $tarr;
    }

    /**
     * all table column
     */
    public static function column()
    {
        $arr1 = self::categorytable();
        $arr2 = self::listtable();
        $arr = array_flip(array_merge($arr2, $arr1));
        return $arr;
    }

    /**
     * all table column and user input(where)
     */
    public static function usercolumn()
    {
        $arr1 = self::column();
        $arr2 = array(
            "userno" => "uid",
            "d1" => "ldatetime",
            "d2" => "ldatetime",
            "usertype" => "utype"
            );
        $arr = array_merge($arr2, $arr1);
        return var_dump($arr);
    }
}
