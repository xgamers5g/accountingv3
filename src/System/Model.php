<?php
namespace Accounting\System;

use PDO;
use Fruit\Seed;
use Fruit\Config;

class Model extends Seed
{
    /**
    * @param $db 為存連接DB的變數
    */
    public static $db = null;

    /**
    * 設定$db與DB進行連線
    */
    public static function set()
    {
        if (is_null(self::$db)) {
            if (self::getConfig()->getDb() != null) {
                self::$db = self::getConfig()->getDb();
            } else {
                echo "fail";
            }
        }
    }
}
