<?php
namespace Accounting\System;

use Fruit\Seed;

class Controller extends Seed
{
    /**
    * @param $data 儲存前端送來資料的變數
    */
    public static $data = null;

    /**
    * 取得前端送來的資料
    */
    public static function init()
    {
        if (file_get_contents("php://input") != null) {
            //self::$data = file_get_contents("php://input");  //x-www-form-urlencoded
            self::$data = json_decode(file_get_contents("php://input"));
        }
    }

    /**
    * 送出json格式的資料
    * @param $arr 陣列（有Key/Value格式的陣列）
    * @return JSON格式的字串
    */
    public static function send($arr)
    {
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
    }
}
