<?php
namespace Accounting\Controller;

use Accounting\Model\V3ListModel;
use Accounting\Lib\V3Lib;
use Accounting\Controller\V3UserController;

class V3ListController
{
    /**
    * 修改明細
    * @param $uarrs 欲修改的條件
    * @param $id lid 欲修改的明細編號
    * @return 回傳執行結果
    */
    public function update()
    {
        $ucs = V3UserController::select();
        if ($ucs != false && array_keys($uarrs)[0] != "lid") {
            $uarrs = V3Lib::usersets();
            for ($i=0; $i<count($ucs); $i++) {
                $r = V3ListModel::load()->update($uarrs, $ucs[$i]["ucid"]);
            }
            if ($r == true) {
                return "修改明細成功,共修改".count($ucs)."筆資料";
            } else {
                return "資料輸入錯誤或不完整";
            }
        } else {
            return "無相關資料";
        }
    }

    /**
    * 刪除明細
    * @param $id lid 明細編號
    * @return 成功回傳true，失敗回傳false 
    */
    public function delete($id)
    {
        $r = V3ListModel::load()->delete($id);
        return $r;
    }

    /**
    * 新增明細
    * @param $c 使用者所選的分類
    * @param $n 使用者輸入的明細內容
    * @param $a 使用者輸入的明細金額
    * @param $d 新增的日期，如果沒有填寫會設成預設當天
    * @return 成功回傳新增成功，失敗回傳新增失敗
    */
    public function create($c, $n, $a, $d)
    {
        if ($d == null) {
            $d = date('Y-m-d');
        }
        $r = V3ListModel::create($c, $n, $a, $d);
        if ($r > 0) {
            return $r;
        } else {
            return "新增明細失敗,資料不完整或錯誤";
        }
    }
}
