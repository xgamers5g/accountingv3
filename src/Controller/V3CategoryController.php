<?php
namespace Accounting\Controller;

use Accounting\Model\V3CategoryModel;
use Accounting\Model\V3ListModel;
use Accounting\Lib\V3Lib;

class V3CategoryController
{
    /**
    * 新增分類並檢查是否重複
    * @param $n cname 使用者輸入的分類名稱
    * @param $t ctype 使用者選擇的分類(1是收入,2是支出)
    * @return 如果成功會回傳新增成功
    */
    public function create($n, $t)
    {
        $r = V3CategoryModel::create($n, $t);
        if ($r > 0) {
            return $r;
        } else {
            return "error";
        }

    }

    /**
    * 刪除分類
    * @param $id cid 分類編號
    * @return 成功回傳true，失敗回傳false 
    */
    public function delete($id)
    {
        $r = V3CategoryModel::load()->delete($id);
        return $r;
    }

    /**
    * 修改明細
    * @param $uarrs 欲修改的條件
    * @param $id cid 欲修改的分類編號
    * @return 回傳執行結果
    */
    public function update()
    {
        $ucs = V3UserController::select();
        $uarrs = V3Lib::usersets();
        if ($ucs != false && array_keys($uarrs)[0] != "cid") {
            for ($i=0; $i<count($ucs); $i++) {
                $r = V3CategoryModel::load()->update($uarrs, $ucs[$i]["ucid"]);
            }
            if ($r == true) {
                return "修改分類成功,共修改".count($ucs)."筆資料";
            } else {
                return "資料輸入錯誤或不完整";
            }
        } else {
            return "無相關資料";
        }
    }
}
