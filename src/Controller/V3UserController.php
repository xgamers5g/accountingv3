<?php
namespace Accounting\Controller;

use Accounting\Lib\V3Lib;
use Accounting\Controller\V3ListController;
use Accounting\Controller\V3CategoryController;
use Accounting\Model\V3UserModel;

class V3UserController
{
    private static $uarr = null;
    /**
    * 建構基本設定
    */
    public function __construct()
    {
        $this->LC = new V3ListController;
        $this->CC = new V3CategoryController;
        self::$uarr = V3Lib::userset();
    }

    /**
    * 新增 user table and list table/category table
    * @param self::$uarr 傳入的使用者給的資料
    * @param $arr, $arr2 我們要的資料
    * @param $sc 呼叫user showcategory function 找既有資料
    * @param $F 假參數
    * @param $nslid 呼叫list cerate function        
    * @param $nscid 呼叫category cerate function
    * @param $r 呼叫user create function
    * @return 成功回傳true，失敗回傳false
    */
    public function create()
    {
        if (count(self::$uarr)<=1) {
            die("請輸入正確資料");
        }
        for ($i=1; $i<count(self::$uarr); $i++) {
            if (substr(array_keys(self::$uarr)[$i], 0, 1) == "l") {
                $arr = array("uid", "lname", "lamount", "cid");
                $arr2 = array("uid", "lname", "lamount", "ldatetime", "cid");
                if (array_keys(self::$uarr) == $arr || array_keys(self::$uarr) == $arr2) {
                    $sc = V3UserModel::load()->showcategory(self::$uarr["uid"], $F);
                    for ($i=0; $i<count($sc); $i++) {
                        if (self::$uarr["cid"] == $sc[$i]["cid"]) {
                            $msg = "return";
                        }
                    }
                    if ($msg == "return") {
                        self::$uarr["lname"] = str_replace("\"", "", self::$uarr["lname"]);    //去除""
                        $nslid = $this->LC->create(self::$uarr["cid"], self::$uarr["lname"], self::$uarr["lamount"], self::$uarr["ldatetime"]);
                        if ($nslid > 0) {
                            $r = V3UserModel::load()->create(self::$uarr["uid"], $nslid, 2);
                        }
                        return var_dump($r);
                    } else {
                        return "請輸入正確資料";
                    }
                } else {
                        return "請輸入正確資料";
                }
            } else {
                $arr = array("uid", "cname", "ctype");
                if (array_keys(self::$uarr) == $arr) {
                    $cn = self::$uarr["cname"];
                    $ct = self::$uarr["ctype"];
                    $cn = str_replace("\"", "", $cn);    //去除""
                    $sc = V3UserModel::load()->showcategory(self::$uarr["uid"], $F);
                    if (empty($sc[0])) {    //如果沒有第一筆資料
                        $nscid = $this->CC->create($cn, $ct);
                        if ($nscid > 0) {
                            $r = V3UserModel::load()->create(self::$uarr["uid"], $nscid, 1);
                        }
                    } else {
                        for ($i=0; $i<count($sc); $i++) {
                            if ($cn == $sc[$i]["cname"] && $ct == $sc[$i]["ctype"]) {
                                $msg = "return";
                                return "已有相同名稱且相同屬性的分類";
                            }
                        }
                        if ($msg != "return") {
                            $nscid = $this->CC->create($cn, $ct);
                            if ($nscid > 0) {
                                $r = V3UserModel::load()->create(self::$uarr["uid"], $nscid, 1);
                            }
                        }
                    }
                    return var_dump($r);
                } else {
                        return "請輸入正確資料";
                }
            }
        }
    }

    /**
    * 查詢 user table
    * @param self::$uarr 傳入的使用者給的資料
    * @param $r 呼叫查詢function
    * @return 成功回傳查詢資料---失敗未處理
    */
    public static function select()
    {
        if (count(self::$uarr) == 1 && substr(array_keys(self::$uarr)[0], 0, 1) == "u" || self::$uarr["scolumn"] == "user") {
            $r = V3UserModel::load()->select(self::$uarr["uid"]);
        } elseif (substr(array_keys(self::$uarr)[1], 0, 1) == "l" || self::$uarr["scolumn"] == "list") {
            $r = V3UserModel::load()->showlist(self::$uarr["uid"], self::$uarr);
        } elseif (substr(array_keys(self::$uarr)[1], 0, 1) == "c" || self::$uarr["scolumn"] == "category") {
            $r = V3UserModel::load()->showcategory(self::$uarr["uid"], self::$uarr);
        } else {
            $r = "error";
        }
        if (count($r) == 0) {
            return "請輸入正確資料";
        } else {
            return var_dump($r);
        }
    }

    /**
    * 查詢 金額
    * @param self::$uarr 傳入的使用者給的資料
    * @param $r 呼叫查詢function
    * @return 成功回傳查詢資料---失敗未處理
    */
    public static function selectmoney()
    {
        $r = V3UserModel::load()->showlistmoney(self::$uarr["uid"], self::$uarr);
        return var_dump($r);
    }

    /**
    * 刪除 user table and list table/category table
    * @param $r 呼叫function 查詢使用者輸入資料是否符合user table
    * @param $ud 呼叫user model function 刪除user table
    * @param $lc 呼叫list function 刪除list table
    * @param $cc 呼叫category function 刪除category table        
    * @return 成功回傳刪除筆數，失敗回傳false
    */
    public function delete()
    {
        $r = $this->select();
        if ($r == null) {
            return "沒有資料可刪除";
        }
        for ($i=0; $i<count($r); $i++) {
            $ud = V3UserModel::load()->delete($r[$i]["uid"], $r[$i]["ucid"], $r[$i]["utype"]);
            if ($ud == true && $r[$i]["utype"] == 2) {
                $lc = $this->LC->delete($r[$i]["ucid"]);
                if ($lc == true) {
                    $lcc++;
                }
            } elseif ($ud == true && $r[$i]["utype"] == 1) {
                $cc = $this->CC->delete($r[$i]["ucid"]);
                if ($ccc == true) {
                    $ccc++;
                }
            }
        }
        if ($lcc > 0 || $ccc > 0) {
            return "刪除成功,共刪除".count($r)."筆資料";
        } else {
            return "刪除失敗";
        }
    }

    //--- update 在 list,category,report controllery 撰寫---
    // /**
    // * 修改 list table/category table 資料
    // * @return 執行結果
    // */
    // public function update()    //list ok
    // {
    //     $r = $this->select();
    //     if ($r != false) {
    //         return $this->LC->update();
    //     }
    //     return "無相關資料";
    // }
    //---------------------------------------------------------------
}
