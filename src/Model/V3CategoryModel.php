<?php
namespace Accounting\Model;

use PDO;
use Accounting\System\Model;

class V3CategoryModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
     * 新增一條分類
     * @param $n 為分類名稱
     * @param $t 為分類屬性
     * @return sucess:true or fail:false
     */
    public static function create($n, $t)
    {
        self::set();
        $qs = "INSERT INTO category (cname, ctype) VALUES(?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $n, PDO::PARAM_STR);
        $stmt->bindValue(2, $t, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $r = self::$db->lastInsertId();
            $stmt->closeCursor();
            return $r;
        } else {
            $stmt->closeCursor();
            return false;
        }
    }

   /**
    * 修改符合條件的分類
    * @param $uarrs 傳入設定值(陣列)
    * @param $id cid 為欲修改的分類編號
    * @param $reqs 為設定值
    * @return sucess:true or fail:false
    */
    public function update($uarrs, $id)
    {
        for ($i=0; $i<count($uarrs); $i++) {
            $reqs = $reqs.$as.array_keys($uarrs)[$i]." = ".$uarrs[array_keys($uarrs)[$i]];
            $as = ", ";
        }
        $qs = "UPDATE category SET ".$reqs." WHERE cid = ".$id;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $stmt->closeCursor();
            return true;
        } else {
            $stmt->closeCursor();
            return false;
        }
    }

    /**
     * 刪除分類
     * @param $id cid 使用者欲修改的分類編號     
     * @return sucess:true or fail:false
     */
    public function delete($id)
    {
        $result = null;
        $qs = "DELETE FROM category WHERE cid = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->closeCursor();
        return $result;
    }

    // /**
    //  * 取出所有分類
    //  * @param $s 為排序方式
    //  * @return 回傳明細(二維陣列)
    //  */
    // public static function get($s)
    // {
    //     self::set();
    //     $result = null;
    //     $qs = "SELECT * FROM category ORDER BY ".$s;
    //     $stmt = self::$db->prepare($qs);

    //     if ($stmt->execute()) {
    //         $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //     }
    //     $stmt->closeCursor();
    //     return $result;
    // }
}
