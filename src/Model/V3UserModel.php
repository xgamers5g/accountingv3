<?php
namespace Accounting\Model;

use PDO;
use Accounting\System\Model;

class V3UserModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
    * 查詢user table資料
    * @return user table資料
    */
    public static function select($u)
    {
        $qs = "SELECT * FROM user WHERE uid = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $u, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢user list明細
    * @return user list明細
    */
    public static function showlist($id, $arr)
    {
        self::set();
        $ta = count($arr);
        if ($ta>0) {
            if (isset($arr["scolumn"])) {
                $ta = $ta-1;
            }
            for ($i=1; $i<$ta; $i++) {
                $a = " AND list.";
                $id = $id.$a.array_keys($arr)[$i]." = ".$arr[array_keys($arr)[$i]];
            }
        }
        $qs = "SELECT * 
                FROM user
                JOIN list
                JOIN category 
                WHERE user.ucid = list.lid
                AND user.utype = 2
                AND list.cid = category.cid
                AND user.uid = ".$id;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢user category明細
    * @return user category明細
    */
    public static function showcategory($id, $arr)
    {
        self::set();
        $ta = count($arr);
        if ($ta>0) {
            if ($arr["scolumn"] != null) {
                $ta = $ta-1;
            }
            for ($i=1; $i<$ta; $i++) {
                $a = " AND category.";
                $id= $id.$a.array_keys($arr)[$i]." = ".$arr[array_keys($arr)[$i]];
            }
        }
        $qs = "SELECT * 
                FROM user
                JOIN category 
                WHERE user.ucid = category.cid
                AND user.utype = 1
                AND user.uid = ".$id;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢user list明細
    * @return user list明細
    */
    public static function showlistmoney($id, $arr)
    {
        self::set();
        $ta = count($arr);
        if ($ta>0) {
            if (isset($arr["scolumn"])) {
                $ta = $ta-1;
            }
            if (isset($arr["group"])) {
                $ta = $ta-1;
            }
            for ($i=1; $i<$ta; $i++) {
                $a = " AND ";
                $id = $id.$a.array_keys($arr)[$i]." = ".$arr[array_keys($arr)[$i]];
            }
        }
        if (isset($arr["group"])) {
            $id = $id." GROUP BY ".$arr["group"];
        }
        $qs = "SELECT SUM(list.lamount), list.lname, category.cname, category.ctype, list.cid
                FROM user
                JOIN list
                JOIN category 
                WHERE user.ucid = list.lid
                AND user.utype = 2
                AND list.cid = category.cid
                AND user.uid = ".$id;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        }
        $stmt->closeCursor();
        return $result;
    }
    /**
    * 新增user table資料
    * @param $u uid 為使用者ID
    * @param $c ucid 為明細id或分類id   
    * @param $t utype 來源屬性 1為分類 2為明細
    * @return sucess:true or fail:false
    */
    public static function create($u, $c, $t)
    {
        self::set();
        $qs = "INSERT INTO user (uid, ucid, utype) VALUES (?, ?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $u, PDO::PARAM_INT);
        $stmt->bindValue(2, $c, PDO::PARAM_INT);
        $stmt->bindValue(3, $t, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $stmt->closeCursor();
            return true;
        } else {
            $stmt->closeCursor();
            return false;
        }
    }

    /**
    * 新增user table資料
    * @param $u uid 為使用者ID
    * @param $c ucid 為明細id或分類id   
    * @param $t utype 來源屬性 1為分類 2為明細
    * @return sucess:true or fail:false
    */
    public function delete($u, $c, $t)
    {
        $req = $u;
        if ($c>0) {
            $req = $req." and ucid = ".$c;
        }
        if ($t>0) {
             $req = $req." and utype = ".$t;
        }
        $qs = "DELETE FROM user WHERE uid = ".$req;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $stmt->closeCursor();
                return true;
            }
        } else {
            $stmt->closeCursor();
            return false;
        }
    }
}
