<?php
namespace Accounting\Model;

use PDO;
use Accounting\System\Model;

class V3ListModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
    * 修改符合條件的明細
    * @param $uarrs 傳入設定值(陣列)
    * @param $id lid 為欲修改的明細編號
    * @param $reqs 為設定值
    * @return sucess:true or fail:false
    */
    public function update($uarrs, $id)
    {
        for ($i=0; $i<count($uarrs); $i++) {
            $reqs = $reqs.$as.array_keys($uarrs)[$i]." = ".$uarrs[array_keys($uarrs)[$i]];
            $as = ", ";
        }
        $qs = "UPDATE list SET ".$reqs." WHERE lid = ".$id;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $stmt->closeCursor();
            return true;
        } else {
            $stmt->closeCursor();
            return false;
        }
    }

    /**
    * 刪除符合條件的明細
    * @param $id lid 明細編號
    * @return sucess:true or fail:false
    */
    public function delete($id)
    {
        $result = null;
        $qs = "DELETE FROM list WHERE lid = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢符合條件的明細
    * @param $req 為查詢條件
    * @return 查詢結果
    */
    public static function select($uarr)
    {
        self::set();
        if (count($uarr) == 1) {
            $req = 1;
        }
        for ($i=1; $i<count($uarr); $i++) {
            $req = $req.$a.array_keys($uarr)[$i]." = ".$uarr[array_keys($uarr)[$i]];
            $a = " AND ";
        }
        $qs = "SELECT * FROM list WHERE ".$req;
        $stmt = self::$db->prepare($qs);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        }
        $stmt->closeCursor();
        return $result;
    }

    public static function create($c, $n, $a, $d)
    {
        Model::set();
        $qs = "INSERT INTO `list` (cid, lname, lamount, ldatetime) VALUES (?, ?, ?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $c, PDO::PARAM_INT);
        $stmt->bindValue(2, $n, PDO::PARAM_STR);
        $stmt->bindValue(3, $a, PDO::PARAM_INT);
        $stmt->bindValue(4, $d, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $r = self::$db->lastInsertId();
            $stmt->closeCursor();
            return $r;
        } else {
            $stmt->closeCursor();
            return false;
        }
    }
}
